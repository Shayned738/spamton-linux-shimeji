#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "LIGHTN<<ER!! YOU MUST GIVE ME PERMISSIONS WITH [[sudo]]! LOOK AT ME MAMA, I'M [root]!!!"
  exit
fi

mkdir -p /usr/share/spamton
cp -r ./* /usr/share/spamton/
install -Dm755 ./spamton /usr/bin/
install -Dm755 ./desktop/Spamton.desktop /usr/share/applications/
install -Dm755 ./desktop/spamton.png /usr/share/icons/hicolor/128x128/apps/
install -Dm755 ./desktop/spamton.png /usr/share/icons/hicolor/128x128/apps/
install -Dm644 ./LICENSE -t /usr/share/licenses/spamton

echo "Installed [[NumberOneBestRatedSalesman1997]]!"
