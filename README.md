# Spamton Shimeji for Linux

Spamton from https://www.youtube.com/watch?v=sHJZWkA-4FA, used with permission

Code from https://github.com/asdfman/linux-shimeji

## Install

### Arch Linux
-  `yay -S spamton-shimeji`

### Other

- Install [OpenJDK 8](https://adoptium.net/temurin/releases/?version=8)
- `git clone https://aur.archlinux.org/spamton-shimeji.git`
- `sudo ./install.sh`

## Usage

- Launch from the desktop file (open app list and search "Spamton")
  - Works best on Xorg
  - Need XWayland for Wayland, may not work as expected
- Click tray icon to spawn more, right click tray icon for options
- They freeze sometimes :(

## Preview

### Go watch this video!

https://youtu.be/iEt-ClLb_yU

![GIF demo](https://cdn.discordapp.com/attachments/810799100940255260/1028840279597400084/https___im5.ezgif.com_tmp_ezgif-5-8109c81717.gif)

![Thumbnail](https://cdn.discordapp.com/attachments/810799100940255260/1030271790695583797/New_Project14.png)